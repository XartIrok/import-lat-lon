<?php
// https://maps.googleapis.com/maps/api/geocode/json?address=ul.+Mireckiego+14,+Radom&region=pl&key=YOUR_API_KEY
header('Content-Type: text/html; charset=utf-8');

function getLocation($address, $city, $is_free = true) {
    $serach = slugify(iconv('', 'utf-8', $address)).',+'.slugify(iconv('', 'utf-8', $city));
    $api_key = ''; // your api key 
    
    if ($is_free)
        $url = "http://maps.googleapis.com/maps/api/geocode/json?address=".$serach."&region=pl";
    else
        $url = "https://maps.googleapis.com/maps/api/geocode/json?address=".$serach."&region=pl&key=".$api_key;

    $cSession = curl_init(); 
    curl_setopt($cSession, CURLOPT_URL, $url);
    curl_setopt($cSession, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($cSession, CURLOPT_HEADER, false); 
    $result=curl_exec($cSession);
    curl_close($cSession);

    $json = json_decode($result, true);

    //printf(var_dump($result, $json['status'])); // debug result info from api

    if ($json['status'] == 'ZERO_RESULTS')
        return 'Brak wyniku';
    elseif ($json['status'] == 'OVER_QUERY_LIMIT')
        return 'Wyczerpany limit dla API';
    else
        return $json['results'][0]['geometry']['location'];
}

function slugify($text) {
    $replace = [
        '&lt;' => '', '&gt;' => '', '&#039;' => '', '&amp;' => '', '&quot;' => '', 
        'Ą' => 'A', 'Ć' => 'C', 'Ę' => 'E', 'Ł' => 'L', 'Ń' => 'N', 'Ó' => 'O', 
        'Ś' => 'S', 'Ż' => 'Z', 'Ź' => 'Z', 'ą' => 'a', 'ć' => 'c', 'ę' => 'e',
        'ł' => 'l', 'ń' => 'n', 'ó' => 'o', 'ś' => 's', 'ż' => 'z', 'ź' => 'z'
    ];

    // make a human readable string
    $text = strtr($text, $replace);

    // replace non letter or digits by -
    $text = preg_replace('~[^\\pL\d.]+~u', '+', $text);

    // trim
    $text = trim($text, '+');
    $text = strtolower($text);
    return $text;
}

$time_start = microtime(true);

$row = 0; $arr = array();
if (($fp = fopen ("import.csv","r")) !== FALSE) {
    while (($data = fgetcsv($fp, 1000, ";")) !== FALSE)  {
        //printf('Wiersz: '.$row.PHP_EOL);
        $row++;
        $arr[$row]['title']     = iconv('', 'utf-8', $data[0]);
        $arr[$row]['desc']      = iconv('', 'utf-8', $data[1]);
        $arr[$row]['address']   = iconv('', 'utf-8', $data[2]);
        $arr[$row]['city']      = iconv('', 'utf-8', $data[3]);
        
        $getLanLon = getLocation($data[2], $data[3]);
        
        if (!is_array($getLanLon)) {
            printf('Błąd wiersza: '.$row.', wiadmość: '.$getLanLon.PHP_EOL);
            continue; // move to next row
        }
        
        $arr[$row]['lat']       = $data[4] ? $data[4] : $getLanLon['lat'];
        $arr[$row]['lng']       = $data[5] ? $data[5] : $getLanLon['lng'];
    }
    fclose ($fp);
}
unset($arr[0]); $row--;// delete first row with name column

file_put_contents('data.json', json_encode($arr));

$time_end = microtime(true);
$time = $time_end - $time_start;

printf('Przetworzenie '.$row.' wierszy, zajeło '.$time.' sekund'.PHP_EOL);
?>